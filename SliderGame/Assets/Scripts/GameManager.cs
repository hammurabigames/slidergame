﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public float hordeSpeed;
    public float targetSpeed;
    public float hordeSpeedMagnifier;
    public Text scoreText;
    public GameObject scoreTextParent;
    public GameObject hordeGameObject;
    public GameObject sceneLoaderObject;
    public GameObject endOfLevelScreen;
    public GameObject rainbowRoad;
    public GameObject cameraHolderObject;
    public GameObject targetSliderObject;
    public GameObject scoreZonesParentObject;
    public GameObject hordeNeonGradientObject;
    public GameObject hordeShadowObject;
    public GameObject HordeBox;

    public GameObject trailParticleObject;
    public GameObject normalParticleObject;
    public GameObject explosionParticleObject;
    public GameObject targetExplosionParticleObject;
    public GameObject particleParentObject;
    public GameObject partyParticles;
    public GameObject hordeFaultParticleObject;
    public GameObject hordeFaultDeadParticleObject;

    
    public Material greenMat;
    public Slider hordeSlider;
    public Slider targetSlider;
    public Transform[] particleSpawnPositions;
    public Transform[] partyPos;
    public GameObject[] frenzyBackgroundObjects;


    [HideInInspector] public Camera mainCamera;
    [HideInInspector] public bool frenzy;
    [HideInInspector] public bool goingRight;
    [HideInInspector] public bool cameraIsFollowing;
    [HideInInspector] public Vector3 movingWay;
    [HideInInspector] public Vector3 movingForward;
    [HideInInspector] public Vector3 movingRight;
    [HideInInspector] public Vector3 movingLeft;
    [HideInInspector] public int score;//jelly de doğru geçtikçe hiçbir şey olmuyor perfect geçilirse fever barı doluyor
    [HideInInspector] public float hordeSpeedBeforeTurning;

    private GameObject speedIndicatorParticle;
    private float oldHordeSpeed;
    private float defaultHordeSpeed;
    private int faultInARow;
    private int scoreInARow;
    private float particleYRotationForTurn;
    private SceneLoader sceneLoader;

    void Start()
    {
        mainCamera = cameraHolderObject.GetComponentInChildren<Camera>();
        sceneLoader = sceneLoaderObject.GetComponent<SceneLoader>();
        defaultHordeSpeed = hordeSpeed;
        frenzy = false;
        goingRight = false;
        score = 0;
    }

    public void addScore()
    {
        score++;
        scoreInARow++;
        faultInARow = 0;
        if (hordeSpeed < 26 && !frenzy)
        {
            hordeSpeed += hordeSpeedMagnifier;
        }

        //turningVariable
        StartCoroutine(RandomlyInstantiateSpeedIndıcatorParticles(10, 0.15f, -90f, particleYRotationForTurn, 0));
        StartCoroutine(DecreaseCameraOffsetZ());
        StartCoroutine(UIScorePerfect());
        scoreText.text = "X" + score.ToString();
    }

    private IEnumerator UIScorePerfect()
    {
        scoreTextParent.SetActive(true);
        scoreTextParent.GetComponent<Animator>().SetTrigger("UIScore");
        yield return new WaitForSeconds(1f);
        scoreTextParent.SetActive(false);
    }
    public void decreaseScore()
    {
        score--;
        faultInARow++;
        if (scoreInARow >= 1)
        {
            hordeSpeed = defaultHordeSpeed;

            //turningVariable
            StartCoroutine(DefaultCameraOffsetZ());
            scoreInARow = 0;
        }
        else
        {
            hordeSpeed -= hordeSpeedMagnifier / 2;
        }
        scoreText.text ="X"+ score.ToString();
        StartCoroutine(dizzyAnimation());

    }

    public void HordeAnimationTrigger(string triggerName)
    {
        for (int i = 2; i <= hordeGameObject.transform.childCount - 1; i++)

        {
            hordeGameObject.transform.GetChild(i).GetComponent<Animator>().SetTrigger(triggerName);
        }
    }


    public IEnumerator HordeAnimationTriggerTimeLag(string triggerName)
    {
        for (int i = 2; i <= hordeGameObject.transform.childCount - 1; i++)

        {
            hordeGameObject.transform.GetChild(i).GetComponent<Animator>().SetTrigger(triggerName);
            yield return new WaitForSeconds(0.05f);
        }
    }


    public void HordeAnimationBool(string boolName, bool boolValue)
    {
        for (int i = 2; i <= hordeGameObject.transform.childCount - 1; i++)

        {
            hordeGameObject.transform.GetChild(i).GetComponent<Animator>().SetBool(boolName, boolValue);
        }
    }

    public void partyUp()
    {
        for (int i = 0; i < partyPos.Length; i++)
        {
            Instantiate(partyParticles, partyPos[i].position, Quaternion.Euler(-90f, 0, 0));
        }
    }


    //turningVariable DONE
    public void targetDestroyExplosion(Vector3 explosionRotation)
    {
        speedIndicatorParticle = Instantiate(targetExplosionParticleObject, particleSpawnPositions[0].position, Quaternion.Euler(explosionRotation));
        speedIndicatorParticle.transform.parent = particleParentObject.transform;
    }


    //turningVariable
    public void hordeFaultParticelSpawn(Vector3 explosionRotation)
    {
        Debug.Log("hordeFault");
        speedIndicatorParticle = Instantiate(hordeFaultParticleObject,particleSpawnPositions[2].position,Quaternion.Euler(explosionRotation));
        speedIndicatorParticle.transform.parent = particleParentObject.transform;

        speedIndicatorParticle = Instantiate(hordeFaultParticleObject, particleSpawnPositions[3].position, Quaternion.Euler(-explosionRotation));
        speedIndicatorParticle.transform.parent = particleParentObject.transform;

        if (faultInARow == 3)
        {
            speedIndicatorParticle = Instantiate(hordeFaultDeadParticleObject, particleSpawnPositions[1].position,
                                                                               Quaternion.Euler(-hordeGameObject.transform.forward));
            speedIndicatorParticle.transform.parent = particleParentObject.transform;
        }
    }

    public void makeScoreZonesGreen()
    {
        for (int i = 0; i <= scoreZonesParentObject.transform.childCount - 1; i++)
        {
            scoreZonesParentObject.transform.GetChild(i).GetComponent<Animator>().SetTrigger("zoneAllGreen");
        }
    }

    public void frenzySpeedIndicatorExplosion(Vector3 explosionSpawnPos)
    {
        //speedIndicatorParticle = Instantiate(explosionParticleObject,explosionSpawnPos,Quaternion.Euler(0,90f,0));
        speedIndicatorParticle = Instantiate(explosionParticleObject, explosionSpawnPos, Quaternion.Euler(0, 0, 0));
        speedIndicatorParticle.transform.parent = particleParentObject.transform;
    }

    IEnumerator dizzyAnimation()
    {
        HordeAnimationBool("Dizzy", true);
        oldHordeSpeed = hordeSpeed;
        hordeSpeed = 0;
        
        if (faultInARow == 1)
        {
            HordeBox.SetActive(false);
            HordeAnimationTrigger("Dead");
            yield return new WaitForSeconds(2f);
            sceneLoader.LoseScene();
        }
        else
        {
            yield return new WaitForSeconds(0.5f);
            HordeAnimationBool("Dizzy", false);
            hordeSpeed = oldHordeSpeed;
        }
    }

    //turningVariable
    IEnumerator DecreaseCameraOffsetZ()
    {
        if (!goingRight)
        {
            for (int i = 0; i < 10; i++)
            {
                mainCamera.transform.position -= new Vector3(0, 0, 0.05f);
                yield return new WaitForSeconds(0.05f);
            }
        }
        else
        {
            for (int i = 0; i < 10; i++)
            {
                mainCamera.transform.position += new Vector3(0.05f, 0, 0);
                yield return new WaitForSeconds(0.05f);
            }
        }
    }


    //turningVariable
    IEnumerator DefaultCameraOffsetZ()
    {
        if (!goingRight)
        {
            for (int i = 0; i < 40; i++)
            {
                if (cameraHolderObject.transform.position.z <= 0)
                {
                    cameraHolderObject.transform.position += new Vector3(0, 0, 0.05f);
                    yield return new WaitForSeconds(0.05f);
                }
            }
        }
        else
        {
            for (int i = 0; i < 40; i++)
            {
                if (cameraHolderObject.transform.position.x <= 0)
                {
                    cameraHolderObject.transform.position -= new Vector3(0.05f, 0, 0);
                    yield return new WaitForSeconds(0.05f);
                }
            }
        }
    }


    //turningVariable
    public IEnumerator RandomlyInstantiateSpeedIndıcatorParticles(int numberOfWaves, float frequancy, float rotationX, float rotationY, float rotationZ)
    {
        if (!goingRight)
        {
            for (int i = 0; i < numberOfWaves; i++)
            {
                speedIndicatorParticle = Instantiate(trailParticleObject, new Vector3(Random.Range(-3, 2), Random.Range(-3, 0),
                                                    hordeGameObject.transform.position.z + 7), Quaternion.Euler(rotationX, rotationY, rotationZ));
                speedIndicatorParticle.transform.parent = particleParentObject.transform;
                speedIndicatorParticle = Instantiate(normalParticleObject, new Vector3(Random.Range(-3, 2), Random.Range(-3, -1f),
                                                    hordeGameObject.transform.position.z + 7), Quaternion.Euler(rotationX, rotationY, rotationZ));

                speedIndicatorParticle.transform.parent = particleParentObject.transform;
                yield return new WaitForSeconds(frequancy);
            }
        }
        else
        {
            for (int i = 0; i < numberOfWaves; i++)
            {
                speedIndicatorParticle = Instantiate(trailParticleObject, new Vector3(hordeGameObject.transform.position.x-4, Random.Range(-3, 0),
                                                    Random.Range(432,437)), Quaternion.Euler(rotationX, rotationY, rotationZ));
                speedIndicatorParticle.transform.parent = particleParentObject.transform;
                speedIndicatorParticle = Instantiate(normalParticleObject, new Vector3(hordeGameObject.transform.position.x-4, Random.Range(-3, -1f),
                                                    Random.Range(432, 437)), Quaternion.Euler(rotationX, rotationY, rotationZ));

                speedIndicatorParticle.transform.parent = particleParentObject.transform;
                yield return new WaitForSeconds(frequancy);
            }
        }
    }


    //turningVariable
    public IEnumerator turnLeft(GameObject turningObject)
    {
       
        if (turningObject.name == "Horde")
        {
            hordeNeonGradientObject.SetActive(false);
            cameraHolderObject.GetComponent<Animator>().SetTrigger("cameraWhenTurning");
            Time.timeScale = 0.7f;

        }
        for (int i = 0; i < 30; i++)
        {
            //turningVariable
            turningObject.transform.Rotate(0f, -3f, 0f, Space.Self);
            yield return new WaitForSeconds(0.012f);
        }
       
        if (turningObject.name == "Horde")
        {
            hordeNeonGradientObject.SetActive(true);
            Time.timeScale = 1f;
            //turningVariable
            particleYRotationForTurn = -90f;
            goingRight = true;
            hordeSpeed = hordeSpeedBeforeTurning;
        }
        if (turningObject.name == "Target")
        {
            targetSpeed = 12;
        }
    }
    public IEnumerator hordeGraduallyStop()
    {
        hordeNeonGradientObject.SetActive(false);
        HordeBox.transform.localScale = new Vector3(4f, 2f, 4f);
        yield return new WaitForSeconds(0.2f);
        hordeSpeed = 0f;
        StartCoroutine(HordeAnimationTriggerTimeLag("party"));
        partyUp();
        cameraHolderObject.GetComponent<Animator>().SetTrigger("cameraRotate");
        yield return new WaitForSeconds(2f);
        endOfLevelScreen.SetActive(true);
    }

    public IEnumerator hordeFrenzy()
    {
        frenzy = true;
        yield return new WaitForSeconds(0.5f);
        makeScoreZonesGreen();
        //turningVariable
        //StartCoroutine(RandomlyInstantiateSpeedIndıcatorParticles(40, 0.1f, -90f, particleYRotationForTurn, 0));
        //StartCoroutine(RandomlyInstantiateSpeedIndıcatorParticles(40, 0.1f, -90f, particleYRotationForTurn, 0));
        StartCoroutine(RandomlyInstantiateSpeedIndıcatorParticles(40, 0.1f, -90f, 0, 0));
        hordeNeonGradientObject.SetActive(false);
        for (int i = 0; i < 4; i++)
        {
            frenzyBackgroundObjects[i].SetActive(true);

        }
        hordeSpeed = 35f;
        for (int i = 0; i < 10; i++)
        {
            //turningVariable
            //cameraHolderObject.transform.position -= new Vector3(0.1f, 0, 0);//goingR
            //cameraHolderObject.transform.position -= new Vector3(0, 0, 0.1f);//goingS
            cameraHolderObject.transform.position += new Vector3(0.1f, 0, 0);//goingL
            yield return new WaitForSeconds(0.05f);
        }
        HordeAnimationTrigger("party");
    }


    public void goingRightVariables()
    {

    }
}