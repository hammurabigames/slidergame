﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour,  IPointerDownHandler, IDragHandler
{
    public GameObject playerGameObject;
    public float scalingMagnitude;
    public float smoothSpeed;

    private float dragMagnitude;
    private float totalSpace;//the amount of space of our box collider
    private Vector3 previousTouchPos;
    private Vector3 scale;//the variable we use for making chnages on scale of box collider

    private float minScaleSize = 1f;
    private float maxScaleSize = 5f;
    private Vector2 screenSize = new Vector2(Screen.width, Screen.height);


    private void Start()
    {
        scale = playerGameObject.transform.localScale;
        totalSpace = scale.x * scale.z;//the amount of space of our box collider
    }

    public void OnPointerDown(PointerEventData eventData)
    {
            previousTouchPos =Input.mousePosition;
            previousTouchPos.z = 0;
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector3 dragPos = Input.mousePosition;//while player is dragging every frame we get his new location
        dragPos.z = 0;
        float distanceY = dragPos.y - previousTouchPos.y;//the  Y distance change according to the earlier frame
        dragMagnitude = distanceY / (screenSize.y / 4);//a value based on the screen of the user that we use in scaling

        scale.x = Mathf.Clamp(scale.x - (scalingMagnitude * dragMagnitude), minScaleSize, maxScaleSize);
        scale.z = totalSpace / scale.x;

        playerGameObject.transform.localScale = Vector3.Lerp(playerGameObject.transform.localScale, scale, smoothSpeed);

        previousTouchPos = dragPos;//when all the other stages are finished we assign the new position value to the ald one to be used when called next frame
    }

}
