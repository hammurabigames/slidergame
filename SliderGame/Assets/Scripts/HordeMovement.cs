﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HordeMovement : MonoBehaviour{

    public GameObject gameManagerObject;
    public GameObject targetObject;
    public Slider slider;
    public Slider targetSlider;

    private GameManager gm;

    private void Start()
    {
        gm = gameManagerObject.GetComponent<GameManager>();
    }

    private void Update()
    {
       transform.position += Time.deltaTime * transform.forward * gm.hordeSpeed;
       targetObject.transform.position += Time.deltaTime * targetObject.transform.forward * gm.targetSpeed;

        //turningVariable
        slider.value = transform.position.z;
       targetSlider.value = targetObject.transform.position.z;
    }
}

