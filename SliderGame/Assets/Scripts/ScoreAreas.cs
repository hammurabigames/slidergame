﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreAreas : MonoBehaviour
{ 
    public float minXValueOfArea;
    public float maxXValueOfArea;

    private GameManager gm;

    private void Start()
    {
        gm = GetComponentInParent<GameManager>();
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "HordeTail")//when the score zone collider collides with the tail of the horde
        {
            if (gameObject.name == "FinishLine")
            {
                StartCoroutine(gm.hordeGraduallyStop());
            }
            else if (gameObject.name != "Target"&& gameObject.name != "turnLine" && gameObject.name != "slowRamp")
            {
                if (col.transform.lossyScale.x >= minXValueOfArea && col.transform.lossyScale.x <= maxXValueOfArea || gm.frenzy)
                {//we control the x values of the horde to be able to tell if the player has sucessfully passed or not
                    gameObject.GetComponent<Animator>().SetTrigger("zoneScored");
                    gm.addScore();
                    gameObject.GetComponent<Collider>().enabled = false;
                    if (gm.frenzy)
                    {
                        //gm.frenzySpeedIndicatorExplosion(gameObject.transform.position + new Vector3(-3, 1.5f, 0f));
                        gm.frenzySpeedIndicatorExplosion(gameObject.transform.position + new Vector3(0, 1.5f, 5f));
                    }
                }
                else
                {
                    gameObject.GetComponent<Animator>().SetTrigger("zoneError");
                    gm.decreaseScore();

                    //turningVariable
                    Debug.Log(col.gameObject.transform.forward);
                    gm.hordeFaultParticelSpawn(col.gameObject.transform.forward);
                    gameObject.GetComponent<Collider>().enabled = false;
                }
            }
        }
        if (gameObject.tag == "Target")
        {
            if (col.gameObject.tag == "HordeHead")
            {
                Destroy(gm.targetSliderObject);
                gm.hordeSlider.transform.GetChild(2).GetComponentInChildren<Image>().color = Color.green;
                gm.hordeSlider.transform.GetChild(1).GetComponentInChildren<Image>().color = Color.green;
                //turningVariable DONE
                gm.targetDestroyExplosion(-col.gameObject.transform.forward);
                GetComponent<Animator>().SetTrigger("Boom");
                StartCoroutine(gm.hordeFrenzy());
                
            }

            //turningVariable
            if (col.gameObject.tag == "turnLine")
            {
                StartCoroutine(gm.turnLeft(gameObject));
            }
            if (col.gameObject.tag == "slowRamp")
            {
                gm.hordeSpeedBeforeTurning = gm.hordeSpeed;
                gm.targetSpeed = 14f;
            }
        }
        if (col.gameObject.tag == "HordeHead")
        {
            if (gameObject.name == "turnLine")
            {
                gameObject.GetComponent<Collider>().enabled = false;
                StartCoroutine(gm.turnLeft(gm.hordeGameObject));
            }
            if (gameObject.tag == "slowRamp")
            {
                gm.hordeSpeedBeforeTurning = gm.hordeSpeed;
                gm.hordeSpeed = 14;
            }
        }
    }
}
